package com.gmail.aliveairush.catservice.listeners;

import com.gmail.aliveairush.catservice.services.CatsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CatQueueListener {


    Logger logger = LoggerFactory.getLogger(CatQueueListener.class);

    private final CatsService catsService;

    @Autowired
    public CatQueueListener(CatsService catsService) {
        this.catsService = catsService;
    }

    /**
     * @param username user who asked wor a cat
     * @return catUrl
     */
    @RabbitListener(queues = "cat-queue")
    public String getCat(String username) {
        logger.info("User with username \"" + username + "\" asked for a cat : ");
        return catsService.getCat().getUrl();
    }

}
