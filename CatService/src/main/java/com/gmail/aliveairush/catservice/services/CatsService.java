package com.gmail.aliveairush.catservice.services;

import com.gmail.aliveairush.catservice.dto.CatDto;

public interface CatsService {
    CatDto getCat();
}
