package com.gmail.aliveairush.catservice.controller;

import com.gmail.aliveairush.catservice.dto.CatDto;
import com.gmail.aliveairush.catservice.services.CatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CatController{

    @Autowired
    private CatsService service;

    @GetMapping("cat/search")
    public ResponseEntity<CatDto> getCat() {
        return ResponseEntity.ok(service.getCat());
    }
}
