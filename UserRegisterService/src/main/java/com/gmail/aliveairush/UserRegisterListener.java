package com.gmail.aliveairush;

import com.gmail.aliveairush.dto.User;
import com.gmail.aliveairush.repositories.UserRpository;
import com.google.gson.Gson;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterListener {

    private final RabbitTemplate rabbitTemplate;

    private final UserRpository repository;

    @Autowired
    public UserRegisterListener(RabbitTemplate rabbitTemplate, UserRpository repository) {
        this.rabbitTemplate = rabbitTemplate;
        this.repository = repository;
    }

    private Gson gson = new Gson();

    // REWORK
    @RabbitListener(queues = "user-queue")
    public String process(String userJson) {
        User newUser = gson.fromJson(userJson, User.class);
        if (repository.findByLogin(newUser.getLogin()) == null){

            repository.save(newUser);
            return newUser.getLogin();
        }
        return "ERROR";
    }
}
