package com.gmail.aliveairush.dto;

import javax.persistence.*;

@Table(name = "users")
@Entity(name = "User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login", unique = true)
    private String login;

    @Column(name = "catUrl", unique = true)
    private String catUrl;

    @Column(name = "pass", unique = true)
    private String pass;

    public String getPass() {
        return pass;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", catUrl='" + catUrl + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getCatUrl() {
        return catUrl;
    }

    public void setCatUrl(String catUrl) {
        this.catUrl = catUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}

