package com.gmail.aliveairush.repositories;

import com.gmail.aliveairush.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRpository extends JpaRepository<User, Long> {
    User findByLogin(String login);
}
