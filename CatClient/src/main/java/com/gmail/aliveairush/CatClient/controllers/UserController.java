package com.gmail.aliveairush.CatClient.controllers;

import com.gmail.aliveairush.CatClient.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.client.RestTemplate;

@Controller
public class UserController {

    private final RestTemplate restTemplate;

    @Value("${user.service.get.request.url}")
    private String userServiceRequestUrl;

    @Autowired
    public UserController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/users")
    public String getUserList(@ModelAttribute("model") ModelMap modelMap){
        UserDto[] userArray = restTemplate.getForEntity(userServiceRequestUrl, UserDto[].class).getBody();
        modelMap.addAttribute("userList", userArray);
        return "user-list";
    }
}
