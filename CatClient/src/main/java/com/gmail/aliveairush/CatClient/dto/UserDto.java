package com.gmail.aliveairush.CatClient.dto;

public class UserDto {
    private Long id;

    private String login;

    private String catUrl;

    private String pass;

    public String getPass() {
        return pass;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", catUrl='" + catUrl + '\'' +
                ", pass='" + pass + '\'' +
                '}';
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getCatUrl() {
        return catUrl;
    }

    public void setCatUrl(String catUrl) {
        this.catUrl = catUrl;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
