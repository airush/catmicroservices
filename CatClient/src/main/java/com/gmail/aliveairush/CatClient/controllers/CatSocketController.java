package com.gmail.aliveairush.CatClient.controllers;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class CatSocketController {

    private final SimpMessagingTemplate messagingTemplate;

    private final RabbitTemplate rabbitTemplate;

    @Autowired
    public CatSocketController(SimpMessagingTemplate messagingTemplate, RabbitTemplate rabbitTemplate) {
        this.messagingTemplate = messagingTemplate;
        this.rabbitTemplate = rabbitTemplate;
    }

    @MessageMapping("/getCat")
    public void sendReply(MessageHeaders messageHeaders, @Payload String message, @Header(name = "simpSessionId") String sessionId){
        // GetFromCatService catUrl
        String catUrl = (String) rabbitTemplate.convertSendAndReceive("micro-exchange", "meow", sessionId);

        // Send to user CatUrl throw sessionId
        messagingTemplate.convertAndSendToUser(sessionId, "/queue/catResponse", catUrl, messageHeaders);
    }

    @MessageMapping("/register")
    public void registerUser(MessageHeaders messageHeaders, @Payload String userJson, @Header(name = "simpSessionId") String sessionId){
        // send to save user. And getHis login
        String userLogin = (String) rabbitTemplate.convertSendAndReceive("micro-exchange", "checkin", userJson);

        // Send to user hisLogin back to inform him, that his profile saved
        messagingTemplate.convertAndSendToUser(sessionId, "/queue/registerResponse", userLogin, messageHeaders);
    }
}
