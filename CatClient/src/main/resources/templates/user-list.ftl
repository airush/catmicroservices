<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Users</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/stylesheet.css" rel="stylesheet">

</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12 my-3">
            <div class="pull-right">
                <div class="btn-group">
                    <button class="btn btn-info" id="list">
                        List View
                    </button>
                    <button class="btn btn-danger" id="grid">
                        Grid View
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="products" class="row view-group">
        <#list model.userList as user>
        <div class="item col-xs-4 col-lg-4">
            <div class="thumbnail card">
                <div class="img-event">
                    <img class="group list-group-image img-fluid" src="${user.catUrl}"/>
                </div>
                <div class="caption card-body">
                    <h4 class="group card-title inner list-group-item-heading">
                        ${user.login}
                    </h4>
                </div>
            </div>
        </div>
        </#list>
    </div>

    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/jquery-3.3.1.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
            $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
        });
    </script>
</div>
</body>
</html>