package com.gmail.aliveairush.controllers;

import com.gmail.aliveairush.dto.User;
import com.gmail.aliveairush.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserTestController {

    private final UserRepository userRepository;

    @Autowired
    public UserTestController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/users")
    public ResponseEntity<List<User>> getTest() {
        return ResponseEntity.ok(userRepository.findAll());
    }
}
